var sounds = document.querySelectorAll('.sound')
var pads = document.querySelectorAll('.pads div')
var visual = document.querySelector('.visual')

var colors = [
    'violet',
    'indigo',
    'blue',
    'green',
    'yellow',
    'orange'
]

pads.forEach((pad, index) => {
    pad.addEventListener('click', function(){
        playAudio(sounds[index])
        createBubbles(index)
    })
})

function playAudio(audio){
    audio.currentTime = 0
    audio.play()
}

const createBubbles = (i) =>{
    const bubble = document.createElement('div')
    visual.appendChild(bubble)
    bubble.style.backgroundColor = colors[i]
    bubble.style.animation = 'jump 1s ease'
    setTimeout(()=>visual.removeChild(bubble), 1000)
}

